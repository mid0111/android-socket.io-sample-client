package com.mid.android_socketio_sample_client.app;

import android.os.Handler;
import android.util.Log;
import io.socket.IOAcknowledge;
import io.socket.IOCallback;
import io.socket.SocketIO;
import io.socket.SocketIOException;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;

/**
 * Created by mid on 14/04/26.
 */
public class SocketConnector {

    SocketConnector socketConnector;
    SocketIO socket;
    OnMessageHandler messageHandler;

    public interface OnMessageHandler {
        public void onMessage(String message);
    }

    public void setOnMessageHandler(OnMessageHandler handler) {
        this.messageHandler = handler;
    }

    public SocketConnector(final Handler handler) throws MalformedURLException {

        socket = new SocketIO("http://192.168.128.236:3000/");
        socket.connect(new IOCallback() {
            public static final String TAG = "HOGE";

            @Override
            public void onMessage(final JSONObject json, IOAcknowledge ack) {
                    handler.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                messageHandler.onMessage(json.toString());
                                Log.i(TAG, "Server said:" + json.toString(2));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            }

            @Override
            public void onMessage(final String data, IOAcknowledge ack) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        messageHandler.onMessage(data);
                        Log.i(TAG, "Server said: " + data);
                    }
                });
            }

            @Override
            public void onError(final SocketIOException socketIOException) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "an Error occured");
                        socketIOException.printStackTrace();
                    }
                });
            }

            @Override
            public void onDisconnect() {
                Log.i(TAG, "Connection terminated.");
            }

            @Override
            public void onConnect() {
                Log.i(TAG, "Connection established");
            }

            @Override
            public void on(final String event, IOAcknowledge ack, Object... args) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        messageHandler.onMessage(event);
                        Log.i(TAG, "Server triggered event '" + event + "'");
                    }
                });
            }

        });
    }

    public void send(String message) {
        // This line is cached until the connection is establisched.
        socket.send("Hello Server!");

    }

}
